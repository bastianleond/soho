<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserPersonal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->id == auth()->user()->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:users,id',
            'name' => 'required|string|max:200|min:2',
            'email' => 'required|string|email|max:200|unique:users,email,'.$this->id,
            'username' => 'required|string|max:200|unique:users,username,'.$this->id,
        ];
    }

    public function messages()
    {
        return [
            'id.exists'=>'El id ingresado es incorrecto',
            'name.required' => 'El nombre es obligatorio',
            'name.string' => 'El nombre debe ser una cadena de caracteres',
            'name.max' => 'El nombre debe tener un máximo de 200 caracteres',
            'name.min' => 'El nombre debe tener un mínimo de 2 caracteres',
            'email.required' => 'El correo es obligatorio',
            'email.string' => 'El correo debe ser una cadena de caracteres',
            'email.max' => 'El correo debe tener un maximo de 200 caracteres',
            'email.unique' => 'El correo ya se encuentra en uso',
            'username.required' => 'El nombre de usuario es obligatorio',
            'username.string' => 'El nombre de usuario debe ser una cadena de caracteres',
            'username.max' => 'El nombre de usuario debe tener un maximo de 200 caracteres',
            'username.unique' => 'El nombre de usuario ya se encuentra en uso',
        ];
    }
}
