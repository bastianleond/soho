<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->rol == 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:products,id',
            'titulo' => 'required',
            'descripcion' => 'required',
            'foto' => 'required|formats'
        ];
    }

    public function messages(){
        return [
            'titulo.required'=>'El titulo es obligatorio',
            'titulo.descripcion'=>'La descripción es obligatoria',
            'foto.required'=>'La foto es obligatoria',
            'foto.formats' => 'La foto  debe ser en formato png, jpeg, jpg o bmp',
            'id.required'=>'El id es requerido',
            'id.exists'=>'El id ingresado es incorrecto'
        ];
    }
}
