<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($product){

                return [
                    'id' => $product->id,
                    'nombre' => $product->nombre,
                    'descripcion' => $product->descripcion,
                    'foto' => $product->url,
                    'created' => $product->created_at->format('d/m/Y H:i:s'),
                    'updated' => $product->updated_at->format('d/m/Y H:i:s'),
                    'usuario' => User::find($product->user_id),
                ];

            })
        ];

    }
}
