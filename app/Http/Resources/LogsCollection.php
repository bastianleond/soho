<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\ResourceCollection;

class LogsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($log){

                return [
                    'id' => $log->id,
                    'log' => $log->log,
                    'created' => $log->created_at->format('d/m/Y H:i:s'),
                    'updated' => $log->updated_at->format('d/m/Y H:i:s'),
                    'usuario' => User::find($log->user_id),
                ];

            })
        ];

    }
}
