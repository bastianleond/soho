<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserPersonal;
use App\Http\Requests\UpdateUserPersonalPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function updatePersonal(UpdateUserPersonal $request){
        $user = User::find(trim($request->input('id')));
        $user->name = ucwords(strtolower(trim($request->input('name'))));
        $user->email = strtolower(trim($request->input('email')));
        $user->username = strtolower(trim($request->input('username')));
        $user->save();
        storeLog('Actualización de datos personales');

        return response()->json([
            'success' => true,
            'user' => $user
        ], 200);
    }

    public function updatePassword(UpdateUserPersonalPassword $request){
        $user = User::find(trim($request->input('id')));
        $user->password = Hash::make(trim($request->input('password')));
        $user->save();
        storeLog('Actualización de contraseña');

        return response()->json([
            'success' => true,
        ], 200);
    }

    public function validPassword(Request $request){
        $user = User::find(trim($request->input('id')));
        $plain = trim($request->input("password"));
        if (Hash::check($plain,$user->password)) {
            return response()->json([
                'success' => true,
            ], 200);
        }else{
            return response()->json([
                'success' => $request->all(),
            ], 403);
        }
    }
}
