<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRegister;
use App\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login','register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $login = request()->input('username');
        $type = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $credentials = [
            $type => $login,
            'password' => request()->input('password')
        ];
        //$credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        storeLog('Inicio de sesion');
        return $this->respondWithToken($token);
    }

    public function register(UserRegister $request){

        $user = new User();
        $user->name = ucwords(strtolower(trim($request->input('name'))));
        $user->email = strtolower(trim($request->input('email')));
        $user->username = strtolower(trim($request->input('username')));
        $user->password = Hash::make(trim($request->input('password')));
        $user->rol = 2;
        $user->estado = 1;
        $user->save();

        return response()->json([
            'success' => true,
        ], 200);

    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }
    public function payload()
    {
        return response()->json(auth()->payload());
    }
    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        storeLog('Salir del sistema');

        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        //Solo en caso de tener vencimiento en el token
        return $this->respondWithToken(auth()->refresh());
    }
    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user(),
        ]);
    }
}
