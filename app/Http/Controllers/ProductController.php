<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteProduct;
use App\Http\Requests\StoreProduct;
use App\Http\Requests\UpdateProduct;
use App\Http\Resources\ProductsCollection;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt',['except' => ['indexWeb']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ProductsCollection(Product::orderBy('id','DESC')->paginate(5));
    }
    public function indexWeb()
    {
        return new ProductsCollection(Product::orderBy('id','DESC')->paginate(20));
    }

    public function count(){
        return response()->json([
            'total' => Product::count()
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProduct $request)
    {
        $product = new Product;
        $product->nombre = trim($request->input('titulo'));
        $product->descripcion = trim($request->input('descripcion'));
        $product->user_id = auth()->user()->id;

        $image = $request->get('foto');
        $exp = explode(',',$image);
        $image = str_replace(' ', '+', $exp[1]);
        $imageName = time().'.'.'png';
        \File::put(public_path(). '/imagenes/productos/' . $imageName, base64_decode($image));
        $product->url = $imageName;

        $product->save();
        storeLog('Creación de producto: '.$product->nombre);
        return response()->json([
            'message' => 'Producto registrado exitosamente'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProduct $request)
    {
        $product = Product::find($request->input('id'));
        $product->nombre = trim($request->input('titulo'));
        $product->descripcion = trim($request->input('descripcion'));

        if($request->get('foto')!== $product->url){
            if (file_exists(public_path().'/imagenes/productos/'.$product->url)) {
                unlink(public_path().'/imagenes/productos/'.$product->url);
            }
            $image = $request->get('foto');  // your base64 encoded
            $exp = explode(',',$image);
            $image = str_replace(' ', '+', $exp[1]);
            $imageName = time().'.'.'png';
            \File::put(public_path(). '/imagenes/productos/' . $imageName, base64_decode($image));
            $product->url = $imageName;

        }

        $product->save();

        storeLog('Actualización de producto: '.$product->nombre);



        return response()->json([
            'message' => 'Producto actualizado exitosamente'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteProduct $request)
    {
        $product = Product::find($request->input('id'));

        if(unlink(public_path().'/imagenes/productos/'.$product->url)){
        }
        storeLog('Eliminación de producto: '.$product->nombre);

        $product->delete();

        return response()->json([
            'message' => 'Producto eliminado exitosamente'
        ],200);
    }
}
