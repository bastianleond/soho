<?php

use App\Log;

function storeLog($action){
    $log = new Log;
    $log->log = $action;
    $log->user_id = auth()->user()->id;
    $log->save();
}
