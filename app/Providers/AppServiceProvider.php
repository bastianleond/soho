<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('formats',function($attribute, $value, $params, $validator) {
            $formats = [
                'jpeg',
                'JPEG',
                'png',
                'PNG',
                'jpg',
                'JPG',
                'bmp',
                'BMP'
            ];
            if(strpos($value,'base64')){
                $exp = explode(',',$value);
                $exp1 = explode(';',$exp[0]);
                $split = explode('/',$exp1[0]);
                return in_array($split[1],$formats);
            }
            if(strpos($value,'.png')){
                return true;
            }
            return false;
        });
    }
}
