<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('register', 'AuthController@register');
    Route::post('me', 'AuthController@me');
});


Route::group([
    'prefix' => 'user',
], function () {
    Route::put('update', 'UserController@updatePersonal');
    Route::put('updatePassword', 'UserController@updatePassword');
    Route::post('validPassword', 'UserController@validPassword');
});


Route::group([
    'prefix' => 'productos',
], function () {
    Route::get('index', 'ProductController@index');
    Route::get('indexWeb', 'ProductController@indexWeb');
    Route::get('count', 'ProductController@count');
    Route::post('store', 'ProductController@store');
    Route::delete('delete', 'ProductController@destroy');
    Route::put('update', 'ProductController@update');
});

Route::group([
    'prefix' => 'registros',
], function () {
    Route::get('index', 'LogController@index');

});
