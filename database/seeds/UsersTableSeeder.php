<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->email = "admin@admin.cl";
        $user->username = "admin";
        $user->estado = 1;
        $user->password = bcrypt("112233");
        $user->name = "Administrador";
        $user->rol = 1;
        $user->save();
    }
}
