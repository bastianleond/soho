<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Soho Project</title>
    </head>

    <body>
        <div id="app">
            <App></App>
        </div>
    <script src="{{ asset('js/app.js') }}"></script>
    </body>

</html>
