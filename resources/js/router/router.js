import Vue from 'vue';
import VueRouter from 'vue-router';
import Inicio from '../componentes/web/Inicio';
import Login from '../componentes/auth/Login';
import Logout from '../componentes/auth/Logout';
import Panel from '../componentes/admin/Panel';
import Registro from '../componentes/auth/Registro';
import Cuenta from '../componentes/web/Cuenta';
import Productos from '../componentes/admin/Productos';
import Registros from '../componentes/admin/Registros';
import ProductosWeb from '../componentes/web/Productos';

//import AdminComponent from './components/admin/AdminComponent';
//import InicioComponent from './components/InicioComponent';
//import GaleriaComponent from './components/admin/GaleriaComponent';
//import UsuariosComponent from './components/admin/UsuariosComponent';
Vue.use(VueRouter);

const router = new VueRouter({
    mode:'history',
    base: __dirname,
    routes:[
        {
            path:'/',
            name:'root',
            component: Inicio,
            meta:{
                layout:'web',
                title:'Soho-Project'
            }
        },

        {
            path:'/login',
            name:'login',
            component: Login,
            meta:{
                requiresVisitor:true,
                layout:'web',
                title:'Soho-Project | Ingreso'
            }
        },
        {
            path:'/registro',
            name:'registro',
            component: Registro,
            meta:{
                requiresVisitor:true,
                layout:'web',
                title:'Soho-Project | Registro'
            }
        },
        {
            path:'/cuenta',
            name:'cuenta',
            component: Cuenta,
            meta:{
                requiresAuth:true,
                layout:'web',
                title:'Soho-Project | Mi Cuenta'
            }
        },
        {
            path:'/productos',
            name:'productos_web',
            component: ProductosWeb,
            meta:{
                layout:'web',
                title:'Soho-Project | Productos'
            }
        },
        {
            path:'/logout',
            name:'logout',
            component: Logout,
            meta:{
                requiresAuth:true,
                title:'Soho-Project | Salir'

            }
        },
        {
            path:'/a/panel',
            name:'panel',
            component: Panel,
            meta:{
                roles:[1],
                requiresAuth:true,
                title:'Soho-Project | Panel'
            }
        },
        {
            path:'/a/productos',
            name:'admin_productos',
            component: Productos,
            meta:{
                requiresAuth:true,
                roles:[1],
                layout:'admin',
                title:'Soho-Project | Admin | Productos'
            }
        },
        {
            path:'/a/registros',
            name:'registros',
            component: Registros,
            meta:{
                requiresAuth:true,
                roles:[1],
                layout:'admin',
                title:'Soho-Project | Admin | Registros'
            }
        },
        {
            path:'*',
            redirect:{name:'root'}
        },

    ]
});

export default router;
