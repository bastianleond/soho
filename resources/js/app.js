
require('./bootstrap');

window.Vue = require('vue');
import router from './router/router.js';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import locale from 'element-ui/lib/locale/lang/es'
import { store } from './store/index.js'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Snotify, { SnotifyPosition } from 'vue-snotify'
import VueProgressBar from 'vue-progressbar'


const SnotifyOptions = {
    toast: {
        position: SnotifyPosition.rightTop
    }
}
const options = {
    color: '#007bff',
    failedColor: '#87111d',
    thickness: '5px',
    transition: {
        speed: '0.2s',
        opacity: '0.6s',
        termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
}

Vue.use(VueProgressBar, options)
Vue.use(Snotify, SnotifyOptions)
Vue.use(ElementUI, { locale });
Vue.use(BootstrapVue)



Vue.component('App', require('./App.vue').default);

router.beforeEach((to,from,next)=>{
    document.title = to.meta.title
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if(!store.getters.loggedIn){
        next({
          name: 'login',
        })
      }
    }
    if (to.matched.some(record => record.meta.requiresVisitor)) {
      if(store.getters.loggedIn){
        next({
          name: 'root',
        })
      }
    }
    const authorization = to.matched.some(record => {
        return record.meta.roles && !record.meta.roles.includes(store.getters.user.rol)
    })
    //Si la autorizacion es true, es porque salio erronea
    if(authorization){
        next({
            name: 'root',
        })
    }

    next()
 })

const app = new Vue({
    el: '#app',
    store,
    router
});
