'use strict'

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    layout: 'web',
    access_token:   localStorage.getItem('access_token') || null,
    user : JSON.parse(localStorage.getItem('user')) || null,
  },
  mutations: {
    SET_LAYOUT (state, payload) {
      state.layout = payload
    },
    SET_USER(state,user){
        state.user = user
    },
    retrieveToken(state,token){
      state.access_token = token.access_token
      state.user= token.user
    },
    destroyToken(state){
      state.access_token = null
      state.user = null
    }
  },
  getters: {
    layout (state) {
      return state.layout
    },
    loggedIn(state){
      return state.access_token !== null
    },
    user(state){
      return state.user
    },
    access_token(state){
      return state.access_token
    },
  },
  actions:{
      //funcion para loguear y obtener el token de jwt
    retrieveToken(context, credentials){
      return new Promise((resolve,reject) => {
        axios.post('/api/auth/login',{
            username: credentials.username,
            password: credentials.password
        }).then(response => {
            const token = response.data.access_token
            const user = response.data.user

            const info = {
              access_token:  token,
              user: user,
            }

            localStorage.setItem('access_token',token)
            localStorage.setItem('user',JSON.stringify(user))
            context.commit('retrieveToken',info)
            resolve(response)

        }).catch(error => {
            reject(error)
        })
      })
    },
    //funcion de deslogueo elimina el token jwt
    destroyToken(context){
      if(context.getters.loggedIn){
        axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.access_token
        return new Promise((resolve,reject) => {
          axios.post('/api/auth/logout')
          .then(response => {
            localStorage.removeItem('access_token')
            localStorage.removeItem('user')
            context.commit('destroyToken')
            resolve(response)
          }).catch(error => {
            reject(error)
          })
        })
      }
    },
    //registro de usuario
    registerUser(context,form){
        return new Promise((resolve,reject) => {
            axios.post('/api/auth/register',{
                email: form.email,
                name: form.name,
                username: form.username,
                password: form.password,
                password_confirmation: form.password_confirmation,
            }).then(response => {
                resolve(response)
            }).catch(error => {
                reject(error)
            })
          })
    },
    registerProduct(context,form){
        axios.defaults.headers.common['Authorization'] = 'Bearer '+context.state.access_token
        return new Promise((resolve,reject) => {
            axios.post('/api/productos/store',{
                titulo: form.titulo,
                descripcion: form.descripcion,
                foto: form.foto,
            }).then(response => {
                resolve(response)
            }).catch(error => {
                reject(error)
            })
          })
    },
    destroyProduct(context, id){
        return new Promise((resolve,reject) => {
          axios.delete('/api/productos/delete',
          {
            headers:{
              Authorization: 'Bearer '+context.getters.access_token
            },
            data:{
              id : id
            }
          })
          .then(response => {
            resolve(response)
          })
          .catch(err => {
            reject(err)
          })
        })
      },
    updateUserPersonal(context, user){
      return new Promise((resolve,reject) => {
        axios.put('/api/user/update',{
            id: user.id,
            name: user.name,
            email: user.email,
            username: user.username,
        },{
          headers:{
            Authorization: 'Bearer '+context.getters.access_token
          }
        })
        .then(response => {
            const user = response.data.user
            localStorage.setItem('user',JSON.stringify(user))
            context.commit('SET_USER',user)
            resolve(response)
        })
        .catch(err => {
          reject(err)
        })
      })
    },
    updateProduct(context, product){
        return new Promise((resolve,reject) => {
          axios.put('/api/productos/update',{
              id: product.id,
              titulo: product.titulo,
              descripcion: product.descripcion,
              foto: product.foto,
          },{
            headers:{
              Authorization: 'Bearer '+context.getters.access_token
            }
          })
          .then(response => {
              const user = response.data.user
              localStorage.setItem('user',JSON.stringify(user))
              context.commit('SET_USER',user)
              resolve(response)
          })
          .catch(err => {
            reject(err)
          })
        })
      },
    updateUserPersonalPassword(context, user){
        return new Promise((resolve,reject) => {
          axios.put('/api/user/updatePassword',{
              id: user.id,
              password: user.password,
              password_confirmation: user.password_confirmation,
          },{
            headers:{
              Authorization: 'Bearer '+context.getters.access_token
            }
          })
          .then(response => {
              resolve(response)
          })
          .catch(err => {
            reject(err)
          })
        })
      },

  }
})
